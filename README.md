# Intro

Group Members:
- Chris Lattman
- Chuck Circo
- Rishi Boda
- Emily Adams
- Sydney May

Merge Request (MR) conventions:
- Name your branch `firstname-boex-##`
- Name your MR `BOEX-##: Feature/fix to be added`
- MRs should be approved by (at least) 1 other group member before being merged

# Running Open edX instance

Note: These instructions assume Python is installed on your computer and `pip` is in your path.

Also Docker Desktop must be installed and running on your computer.
- For Windows, make sure to use the WSL 2 backend

First-time installation (one time only):
```
$ pip install tutor tutor-mfe
$ tutor plugins enable mfe
$ tutor local launch
```
- This step takes about 10-20 minutes

Open edX home page is available at
http://local.overhang.io

Open edX Studio is available at
http://studio.local.overhang.io

Stop Open edX:
```
$ tutor local stop
```

Restart Open edX:
```
$ tutor local start -d
```

Create admin user (one time only):
```
$ tutor local do createuser --staff --superuser username user@email.com
```
- It will prompt you to enter a new password for this account

Optional: download Indigo theme (one time only):
```
$ pip install tutor-indigo
$ tutor plugins enable indigo
$ tutor config save
$ tutor images build openedx
$ tutor local do settheme indigo
```

## Compress course folder into one file:

```
tar -czf course.tar.gz course/
```
